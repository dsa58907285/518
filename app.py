
from flask import render_template, Flask
import connexion
import uuid
#from connexion.exceptions import BadRequestProblem
from datetime import datetime
import time
import traceback
import logging
import json

# log setting
logger = logging.getLogger('app_logger')
logger.setLevel(logging.INFO)

# Create handlers
c_handler = logging.StreamHandler()
c_format = logging.Formatter('%(asctime)s %(name)s - %(process)d - %(levelname)s - %(message)s')
c_handler.setFormatter(c_format)

# Add handlers to the logger
logger.addHandler(c_handler)

def get_housedata():
    return "data"

# Create the application instance
app = connexion.App(__name__, specification_dir='./')

# Read the swagger.yml file to configure the endpoints
app.add_api('swagger.yml')

@app.route('/')
def index():
    return "Server working..."

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
    # app.run(debug=True)
