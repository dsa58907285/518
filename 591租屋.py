# -*- coding: utf-8 -*-
"""
Created on Sat Apr 10 08:30:17 2021

@author: JS
"""

import time
import re
from bs4 import BeautifulSoup
import json
import pandas as pd
from urllib.request import urlopen
import re
import requests

def rule(text):
    dr = re.compile(r'<[^>]+>',re.S)
    text = dr.sub('',str(text))  #去內容標籤
    text=text.strip()#去掉空白
    return text

def downloadjson(url):      
#抓取網站html
    headers = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'sec-ch-ua': '^\\^Google',
    'sec-ch-ua-mobile': '?0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
    'Accept': '*/*',
    'Sec-Fetch-Site': 'same-site',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-User': '?1',
    'Sec-Fetch-Dest': 'empty',
    'Referer': 'https://rent.591.com.tw/',
    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
    'Origin': 'https://rent.591.com.tw',
    'authority': 'www.google-analytics.com',
    'content-length': '0',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
    'content-type': 'application/x-www-form-urlencoded',
    'accept': 'image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8',
    'origin': 'https://rent.591.com.tw',
    'sec-fetch-site': 'cross-site',
    'sec-fetch-mode': 'no-cors',
    'sec-fetch-dest': 'image',
    'referer': 'https://rent.591.com.tw/',
    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
    'X-XSRF-TOKEN': 'eyJpdiI6InJCNWpFeW5OSHdNXC9YQzNEbUZRemlRPT0iLCJ2YWx1ZSI6Ik04Mk9raU9vcUJKd1NUYlwvZUprRjAxTjk4b1hvekNXRlpJNTNMd1NLTjdhZXB1MmNCTzVqa0ozOUdSeXIzZ0xwazU0b3RveW1iaWFLY3NIZjVqK2N6dz09IiwibWFjIjoiZmVlNDc1YTdiYWI1ZDE2MGE2M2U1ZWYzZTA2YTEyODk5OGY5OWQ5NTRiMmRlOGZlMzI4NGQ5M2I3NjY4ZmRhNCJ9',
    'X-CSRF-TOKEN': 'wLuPJEWs6zM6AS1MLmyioVyr0w66nW8CmEEAXNbb',
    'X-Requested-With': 'XMLHttpRequest',
    'cookie': 'fr=0M30X7SLwG6mq1xR8..BgcHe8...1.0.BgcHe8.',
    'pragma': 'no-cache',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Access-Control-Request-Method': 'POST',
    'Access-Control-Request-Headers': 'x-csrf-token',
    'Cookie': 'webp=1; PHPSESSID=t5gtp452h4473dn1vfckvtsdd6; new_rent_list_kind_test=0; T591_TOKEN=t5gtp452h4473dn1vfckvtsdd6; _ga=GA1.3.147504705.1618014945; _gid=GA1.3.1008945311.1618014945; _gid=GA1.4.1008945311.1618014945; _ga=GA1.4.147504705.1618014945; tw591__privacy_agree=0; _fbp=fb.2.1618014946007.1101394266; user_index_role=1; __auc=712835f6178b963eec7d77889b3; localTime=2; imgClick=10741327; user_browse_recent=a%3A5%3A%7Bi%3A0%3Ba%3A2%3A%7Bs%3A4%3A%22type%22%3Bi%3A1%3Bs%3A7%3A%22post_id%22%3Bs%3A8%3A%2210494487%22%3B%7Di%3A1%3Ba%3A2%3A%7Bs%3A4%3A%22type%22%3Bi%3A1%3Bs%3A7%3A%22post_id%22%3Bs%3A8%3A%2210628310%22%3B%7Di%3A2%3Ba%3A2%3A%7Bs%3A4%3A%22type%22%3Bi%3A1%3Bs%3A7%3A%22post_id%22%3Bs%3A8%3A%2210639308%22%3B%7Di%3A3%3Ba%3A2%3A%7Bs%3A4%3A%22type%22%3Bi%3A1%3Bs%3A7%3A%22post_id%22%3Bs%3A8%3A%2210690812%22%3B%7Di%3A4%3Ba%3A2%3A%7Bs%3A4%3A%22type%22%3Bi%3A1%3Bs%3A7%3A%22post_id%22%3Bs%3A8%3A%2210674548%22%3B%7D%7D; _gat=1; _dc_gtm_UA-97423186-1=1; _gat_UA-97423186-1=1; urlJumpIp=1; urlJumpIpByTxt=%E5%8F%B0%E5%8C%97%E5%B8%82; XSRF-TOKEN=eyJpdiI6InJCNWpFeW5OSHdNXC9YQzNEbUZRemlRPT0iLCJ2YWx1ZSI6Ik04Mk9raU9vcUJKd1NUYlwvZUprRjAxTjk4b1hvekNXRlpJNTNMd1NLTjdhZXB1MmNCTzVqa0ozOUdSeXIzZ0xwazU0b3RveW1iaWFLY3NIZjVqK2N6dz09IiwibWFjIjoiZmVlNDc1YTdiYWI1ZDE2MGE2M2U1ZWYzZTA2YTEyODk5OGY5OWQ5NTRiMmRlOGZlMzI4NGQ5M2I3NjY4ZmRhNCJ9; 591_new_session=eyJpdiI6IkZVaUJpajlOaU9jbmdPaVBaOVhIQlE9PSIsInZhbHVlIjoiSUszQzA4ajNJdVlWbzRDS3NqS0RFRVd6WDBxZEpXOE8ycDdVNTdkT1ZISWhFSDhJK2c3QnQ4akFDXC9iUHNocEZGbkpWelQ5cVRHY0t0VksyaUlVemRRPT0iLCJtYWMiOiI4ZThjNTBkODVjZDFjYTE3NDBmZGJiMWZlNjIyNmU4NjdlZTAxNzBiMzYwZmU1ZjUxMDUwYjA0Y2JmZGU1NzhiIn0%3D'
    }
    r=requests.get(url,headers=headers)
    if r.status_code==200:
        r.encoding='utf-8'
        return json.loads(r.text)
    return None	

def download(url):  
#抓取網站html
    user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64)'
    headers={'User_Agent':user_agent}
    r=requests.get(url,headers=headers)
    if r.status_code==200:
        r.encoding='utf-8'
        return r.text	
    return None


#houseid
#linkman 出租者
#nick_name 出租者身分  (屋主 羅小姐)
#電話
#型態
#kind_name 現況
savedata=[]
firstRow=0
while(firstRow<1852):
    print(firstRow)
    url='https://rent.591.com.tw/home/search/rsList?is_new_list=1&type=1&kind=0&searchtype=1&region=1&rentprice=0&other=balcony_1,pet&firstRow='+str(firstRow)+'&totalRows=1852'
    data=downloadjson(url)
    jsondata=data['data']['data']
    for j in jsondata:
        #time.sleep(0.1)
        houseid=j['houseid']
        url='https://rent.591.com.tw/rent-detail-'+str(houseid)+'.html'
        html=download(url)
        
        soup=BeautifulSoup(html,features='lxml')
        #nametext=soup.find_all('div',style='margin-top: 13px;')userInfo
        nametext=soup.find_all('div',class_='userInfo')

        try:
            renter_name=nametext[0].find('i').text  #出租者
            renter_name=rule(renter_name) #出租者
        except:
            renter_name=''
        
        try:
            identity=nametext[0].find('div',class_='avatarRight').text  #出租者
#
#            identity=str(nametext[0]).split('（')[-1][:-9]
#            identity=identity.split('：')[-1]  #
#            identity=str(identity).split('）')[0]  #
#            identity=str(identity).split('）')[0]
            identity=rule(identity) #出租者身分
            if "（" in identity:
                identity=str(identity).split('）')[0]
                identity=str(identity).split('（')[-1]
            else:
                identity=str(identity).split(')')[0]
                identity=str(identity).split('(')[-1]

        except:
            identity=''
            
        try:
            housetype=soup.find_all('ul',class_='attr')
            housetype=housetype[0].find_all('li')
            if len(housetype)==5:
                house_type=str(housetype[-3]).split(':')[-1]
                house_type=rule(house_type) #型態
            if len(housetype)==4:
                house_type=str(housetype[-2]).split(':')[-1]
                house_type=rule(house_type) #型態
        except:
            house_type=''
                        
        try:
            if len(housetype)==5:
                house_now=str(housetype[-2]).split(':')[-1]
                house_now=rule(house_now)#現況
            if len(housetype)==4:
                house_now=str(housetype[-1]).split(':')[-1]
                house_now=rule(house_now)#現況
        except:
            house_now=''
        
        try:##性別
            gender=soup.find_all('em',title='男女生皆可')[0].text
        except IndexError:
            try:
                gender=soup.find_all('em',title='不限')[0].text
            except IndexError:
                try:
                    gender=soup.find_all('em',title='男生')[0].text
                except IndexError:
                    try:
                        gender=soup.find_all('em',title='女生')[0].text
                    except IndexError:
                        gender=''
                        
        save={  'houseid':houseid,
                 'city':'台北市',
                'renter_name':renter_name,
                'identity':identity,
                'house_type':house_type,
                'house_now':house_now,
                'gender':gender,
                }
        #print(save)
        savedata.append(save)
    firstRow=firstRow+30
    


columns=['houseid','city','renter_name','identity','house_type','house_now','gender']
finaldata=pd.DataFrame(columns=columns,data=savedata)
finaldata.replace('\s+','',regex=True,inplace=True) 


finaldata.to_excel("output_台北.xlsx")  



















