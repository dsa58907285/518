資料源:
591房屋交易租屋(https://rent.591.com.tw/?kind=0&region=1) 

程式語言：python 

API文件：swagger(API 請使用flask建立)

資料庫：postgresql

題目：
步驟1.利⽤爬網技術取得【591房屋交易租屋網】中，位於【臺北及新北】的所有【租屋物件資料】。

步驟2.【租屋物件資料】至少須具有下列欄位：

    1.出租者(陳先生)
    2.出租者身份(屋主)
    3.型態(電梯大樓)
    4.現況(獨立套房)
    5.性別要求(男女生皆可)

步驟3.將租屋物件資料儲存在資料庫中。

步驟4.執行成果【設計/建立RESTful API】供查詢下列資訊:【以JSON格式回傳，請自訂Schema】

4.1【男生可承租】且【位於新北】的租屋物件
    
![](https://gitlab.com/dsa58907285/518/-/raw/master/image/1.PNG)


4.2 所有【非屋主自行刊登】的租屋物件

![](https://gitlab.com/dsa58907285/518/-/raw/master/image/2.PNG)


4.3【臺北】【屋主為女性】【姓氏為吳】所刊登的所有租屋物件

![](https://gitlab.com/dsa58907285/518/-/raw/master/image/3.PNG)

請【提出或實作營運化方法/機制】，包含但不限於以下需求:

1.資料源更新機制:

    設定排程固定每小時更新，抓取時間區間內新增的資料，如果物件ID不再資料庫內則匯入資料庫中，物件ID在資料庫中則Updata資料。
    

2.資料庫設計:

    利用Docker建立postgresql資料庫做資料的儲存。

3.API的規格文件:

    輸入json格式{renter_name:None,identity:None,city:None,gender:None,house_type:None,house_now:None}
    需要篩選的條件在填入JSON中，不須每個變數都填。

    /------欄位名稱--------
    renter_name  #屋主名稱
    identity #屋主(仲介)
    city #地點
    gender  #限男女
    house_type  #房子型態
    house_now #房子現況
    -------------------/







